import React, { useState } from "react";
import "./LogIn.css";
import "../index.css";
import NavigationBar from "../Navigation/Navigation";
import Network from "../Network";

function LogIn() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");

    const onUsernameChange = (e) => {
        setUsername(e.target.value);
    };

    const onPasswordChange = (e) => {
        setPassword(e.target.value);
    };

    const onSubmit = (e) => {
        e.preventDefault();
        setError("");
        Network.logIn(username, password).then((result) => {
            if (result === "signed user into account") {
                localStorage.setItem("username", username);
                window.location.replace("/");
            } else {
                setError(result);
            }
        })
        .catch((error) => {
            setError(error);
        })
    };

    return (
        <div className="App">
            <NavigationBar />
            <form className="Form-container" onSubmit={onSubmit}>
                <h2 className="Form-title">Log In</h2>
                <p className="Form-error">{error}</p>
                <input
                    className="Form-input"
                    value={username}
                    onChange={onUsernameChange}
                    placeholder="Username"
                />
                <input
                    className="Form-input"
                    value={password}
                    onChange={onPasswordChange}
                    placeholder="Password"
                    type="password"
                />
                <button className="Button Form-submit" type="submit">Submit</button>
            </form>
        </div>
    );
}

export default LogIn;
