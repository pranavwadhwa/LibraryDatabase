import React, { useState } from "react";
import "./DeletePublisher.css";
import "../index.css";
import NavigationBar from "../Navigation/Navigation";
import Network from "../Network";

function DeletePublisherForm() {
    const [result, setResult] = useState("");

    const [publisherData, setPublisherData] = useState({
        publisherName: "",
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setPublisherData({ ...publisherData, [name]: value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        Network.deletePublisher(publisherData.publisherName)
            .then((response) => {
                setResult(response);
                setPublisherData({
                    publisherName: "",
                });
            });
    };

    function setInitialData() {
        setPublisherData({
            publisherName: "Scholastic Inc.",
        });
    }

    return (
        <div className="App">
            <NavigationBar />
            <h2>Delete Publisher</h2>
            <button onClick={setInitialData}>Test with initial values</button>
            <form onSubmit={handleSubmit}>
                <div className="Form-item">
                    <label htmlFor="publisherName">Publisher Name:</label>
                    <input
                        type="text"
                        id="publisherName"
                        name="publisherName"
                        value={publisherData.publisherName}
                        onChange={handleChange}
                        placeholder="Publisher Name"
                    />
                </div>
                <button className="Button" type="submit">Submit</button>
            </form>
            <p>{result}</p>
        </div>
    );
}

export default DeletePublisherForm;
