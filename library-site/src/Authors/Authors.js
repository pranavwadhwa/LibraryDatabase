import React, { useState, useEffect } from "react";
import "./Authors.css";
import "../index.css";
import NavigationBar from "../Navigation/Navigation";
import Network from "../Network";

function AuthorView(author) {
    return (
        <div className="Author-container" key={author.authorName}>
            <h4 className="Author-name">{author.authorName}</h4>
            <p className="Author-bio">{author.bio}</p>
        </div>
    );
}

function Authors() {
    const [authors, setAuthors] = useState([]);
    let fetched = false;

    useEffect(() => {
        if (fetched) return;
        fetched = true;
        Network.getAuthors().then((authors) => {
            setAuthors(authors);
            console.log(authors);
        });
    }, []);

    return (
        <div className="App">
            {NavigationBar()}
            <h1>Authors</h1>
            <div className="Authors-list">
                { authors.map((author) => AuthorView(author))}
            </div>
        </div>
    );
}

export default Authors;
