import React, { useState } from "react";
import "../index.css";
import "./AddBook.css";
import NavigationBar from "../Navigation/Navigation";
import Network from "../Network";

function InsertBookForm() {
    const [result, setResult] = useState("");

    const [bookData, setBookData] = useState({
        book_id: "",
        isbn: "",
        name: "",
        author: "",
        cover_photo_url: "",
        year_published: "",
        publishing_company: "",
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setBookData({ ...bookData, [name]: value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        // const u = new URLSearchParams(bookData).toString();
        // fetch("http://127.0.0.1:5000/insert_book?" + u, {
        //     method: "GET",
        // })
        //     .then((response) => response.text())
        Network.addBook(bookData)
            .then((response) => {
                setResult(response);
                setBookData({
                    book_id: "",
                    isbn: "",
                    name: "",
                    author: "",
                    cover_photo_url: "",
                    year_published: "",
                    publishing_company: "",
                });
            });
    };

    function setInitialData() {
        setBookData({
            book_id: "9780316769488-0",
            author: "J. D. Salinger",
            cover_photo_url:
                "https://images-na.ssl-images-amazon.com/images/W/IMAGERENDERING_521856-T1/images/I/61fgOuZfBGL._AC_UL116_SR116,116_.jpg",
            isbn: "9780316769488",
            name: "Catcher in the Rye",
            publishing_company: "Little, Borwn and Company",
            year_published: "1991",
        });
    }

    return (
        <div className="App">
            <NavigationBar />
            <h2>Insert Book</h2>
            <button onClick={setInitialData}>Test with initial values</button>
            <form className="Form" onSubmit={handleSubmit}>
                <div className="Form-item">
                    <label htmlFor="book_id">Book ID:</label>
                    <input
                        type="text"
                        id="book_id"
                        name="book_id"
                        value={bookData.book_id}
                        onChange={handleChange}
                        placeholder="Book ID"
                    />
                </div>
                <div className="Form-item">
                    <label htmlFor="isbn">ISBN:</label>
                    <input
                        type="text"
                        id="isbn"
                        name="isbn"
                        value={bookData.isbn}
                        onChange={handleChange}
                        placeholder="ISBN"
                    />
                </div>
                <div className="Form-item">
                    <label htmlFor="name">Name:</label>
                    <input
                        type="text"
                        id="name"
                        name="name"
                        value={bookData.name}
                        onChange={handleChange}
                        placeholder="Name"
                    />
                </div>
                <div className="Form-item">
                    <label htmlFor="author">Author:</label>
                    <input
                        type="text"
                        id="author"
                        name="author"
                        value={bookData.author}
                        onChange={handleChange}
                        placeholder="Author"
                    />
                </div>
                <div className="Form-item">
                    <label htmlFor="cover_photo_url">Cover Photo URL:</label>
                    <input
                        type="text"
                        id="cover_photo_url"
                        name="cover_photo_url"
                        value={bookData.cover_photo_url}
                        onChange={handleChange}
                        placeholder="Cover Photo URL"
                    />
                </div>
                <div className="Form-item">
                    <label htmlFor="year_published">Year Published:</label>
                    <input
                        type="text"
                        id="year_published"
                        name="year_published"
                        value={bookData.year_published}
                        onChange={handleChange}
                        placeholder="Year Published"
                    />
                </div>
                <div className="Form-item">
                    <label htmlFor="publishing_company">
                        Publishing Company:
                    </label>
                    <input
                        type="text"
                        id="publishing_company"
                        name="publishing_company"
                        value={bookData.publishing_company}
                        onChange={handleChange}
                        placeholder="Publishing Company"
                    />
                </div>
                <button className="Button" type="submit">Submit</button>
            </form>
            <p>{result}</p>
        </div>
    );
}

export default InsertBookForm;
