import React, { useState } from "react";
import "./DeleteBook.css";
import "../index.css";
import NavigationBar from "../Navigation/Navigation";
import Network from "../Network";

function DeleteBookForm() {
    const [result, setResult] = useState("");

    const [bookData, setBookData] = useState({
        book_id: "",
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setBookData({ ...bookData, [name]: value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        // const u = new URLSearchParams(bookData).toString();
        // fetch("http://127.0.0.1:5000/delete_book?" + u, {
        //     method: "GET",
        // })
        //     .then((response) => response.text())
        Network.deleteBook(bookData.book_id)
            .then((response) => {
                setResult(response);
                setBookData({
                    book_id: "",
                });
            });
    };

    function setInitialData() {
        setBookData({
            book_id: "9780316769488-0",
        });
    }

    return (
        <div className="App">
            <NavigationBar />
            <h2>Delete Book</h2>
            <button onClick={setInitialData}>Test with initial values</button>
            <form onSubmit={handleSubmit}>
                <div className="Form-item">
                    <label htmlFor="book_id">Book ID:</label>
                    <input
                        type="text"
                        id="book_id"
                        name="book_id"
                        value={bookData.book_id}
                        onChange={handleChange}
                    />
                </div>
                <button className="Button" type="submit" placeholder="Book Id">Submit</button>
            </form>
            <p>{result}</p>
        </div>
    );
}

export default DeleteBookForm;
