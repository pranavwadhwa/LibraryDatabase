import { useState } from "react";
import NavigationBar from "../Navigation/Navigation";
import "../index.css";
import "./Account.css";
import Network from "../Network";

function logOut() {
    localStorage.removeItem("username");
    window.location.replace("/");
}

function Account() {
    const [oldPassword, setOldPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [error, setError] = useState("");

    function changePassword() {
        //sucessfuly changed password
        setError("");
        Network.changePassword(
            localStorage.getItem("username"),
            oldPassword,
            newPassword
        ).then((result) => {
            setError(result);
            setOldPassword("");
            setNewPassword("");
        });
    }
    return (
        <div className="App">
            <NavigationBar />
            <div className="Account-container">
                <h3 className="Account-name">Name goes here</h3>
                <h5 className="Account-username">
                    {localStorage.getItem("username")}
                </h5>
                <button onClick={logOut} className="Button">
                    Log Out
                </button>
            </div>
            <div className="Account-container">
                <h3>Change Password</h3>
                <p>{error}</p>
                <input
                    placeholder="Old Password"
                    value={oldPassword}
                    type="password"
                    onChange={(e) => {
                        setOldPassword(e.target.value);
                    }}
                />
                <input
                    placeholder="New Password"
                    value={newPassword}
                    type="password"
                    onChange={(e) => {
                        setNewPassword(e.target.value);
                    }}
                />
                <button onClick={changePassword} className="Button">
                    Change Password
                </button>
            </div>
        </div>
    );
}

export default Account;
