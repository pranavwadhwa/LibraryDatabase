import React, { useState } from "react";
import "../index.css";
import "./AddAuthor.css";
import NavigationBar from "../Navigation/Navigation";
import Network from "../Network";

function AddAuthorForm() {
    const [result, setResult] = useState("");

    const [authorData, setAuthorData] = useState({
        name: "",
        bio: "",
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setAuthorData({ ...authorData, [name]: value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        Network.addAuthor(authorData)
            .then((response) => {
                setResult(response);
                setAuthorData({
                    name: "",
                    bio: ""
                });
            });
    };

    function setInitialData() {
        setAuthorData({
            name: "J.K. Rowling",
            bio: "Joanne Rowling CH OBE FRSL, also known by her pen name J. K. Rowling, is a British author and philanthropist. She wrote Harry Potter, a seven-volume children's fantasy series published from 1997 to 2007."
        });
    }

    return (
        <div className="App">
            <NavigationBar />
            <h2>Insert Author</h2>
            <button onClick={setInitialData}>Test with initial values</button>
            <form className="Form" onSubmit={handleSubmit}>
                <div className="Form-item">
                    <label htmlFor="name">Name:</label>
                    <input
                        type="text"
                        id="name"
                        name="name"
                        value={authorData.name}
                        onChange={handleChange}
                        placeholder="Name"
                    />
                </div>
                <div className="Form-item">
                    <label htmlFor="bio">Bio:</label>
                    <input
                        type="text"
                        id="bio"
                        name="bio"
                        value={authorData.bio}
                        onChange={handleChange}
                        placeholder="Bio"
                    />
                </div>
                <button className="Button" type="submit">Submit</button>
            </form>
            <p>{result}</p>
        </div>
    );
}

export default AddAuthorForm;
