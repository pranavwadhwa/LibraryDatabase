import React, { useState, useEffect } from "react";
import "./Publishers.css";
import "../index.css";
import NavigationBar from "../Navigation/Navigation";
import Network from "../Network";

function PublisherView(publisher) {
    return (
        <div className="Publisher-container" key={publisher.publisher_id}>
            <div className="Publisher-info">
            <h4 className="Publisher-name">{publisher.name}</h4>
            <p className="Publisher-location">{publisher.location}</p>
            </div>
            <a href={publisher.website} className="Publisher-link" target="_blank">View Website</a>
        </div>
    );
}

function Publishers() {
    const [publishers, setPublishers] = useState([]);
    let fetched = false;

    useEffect(() => {
        if (fetched) return;
        fetched = true;
        Network.getPublishers().then((publishers) => {
            setPublishers(publishers);
            console.log(publishers);
        });
    }, []);

    return (
        <div className="App">
            {NavigationBar()}
            <h1>Publishers</h1>
            <div className="Publishers-list">
                { publishers.map((publisher) => PublisherView(publisher))}
            </div>
        </div>
    );
}

export default Publishers;
