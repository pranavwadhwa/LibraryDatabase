import React, { useState } from "react";
import "./DeleteAuthor.css";
import "../index.css";
import NavigationBar from "../Navigation/Navigation";
import Network from "../Network";

function DeleteAuthorForm() {
    const [result, setResult] = useState("");

    const [authorData, setAuthorData] = useState({
        authorName: "",
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setAuthorData({ ...authorData, [name]: value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        Network.deleteAuthor(authorData.authorName)
            .then((response) => {
                setResult(response);
                setAuthorData({
                    authorName: "",
                });
            });
    };

    function setInitialData() {
        setAuthorData({
            authorName: "J.K. Rowling",
        });
    }

    return (
        <div className="App">
            <NavigationBar />
            <h2>Delete Author</h2>
            <button onClick={setInitialData}>Test with initial values</button>
            <form onSubmit={handleSubmit}>
                <div className="Form-item">
                    <label htmlFor="authorName">Author Name:</label>
                    <input
                        type="text"
                        id="authorName"
                        name="authorName"
                        value={authorData.authorName}
                        onChange={handleChange}
                        placeholder="Author Name"
                    />
                </div>
                <button className="Button" type="submit">Submit</button>
            </form>
            <p>{result}</p>
        </div>
    );
}

export default DeleteAuthorForm;
