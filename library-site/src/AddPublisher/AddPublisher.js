import React, { useState } from "react";
import "../index.css";
import "./AddPublisher.css";
import NavigationBar from "../Navigation/Navigation";
import Network from "../Network";

function AddPublisherForm() {
    const [result, setResult] = useState("");

    const [publisherData, setPublisherData] = useState({
        company_name: "",
        website: "",
        location: "",
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setPublisherData({ ...publisherData, [name]: value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        Network.addPublisher(publisherData).then((response) => {
            setResult(response);
            setPublisherData({
                company_name: "",
                website: "",
                location: "",
            });
        });
    };

    function setInitialData() {
        setPublisherData({
            company_name: "Scholastic Inc.",
            website: "https://www.scholastic.com/",
            location: "New York, NY",
        });
    }

    return (
        <div className="App">
            <NavigationBar />
            <h2>Insert Publisher</h2>
            <button onClick={setInitialData}>Test with initial values</button>
            <form className="Form" onSubmit={handleSubmit}>
                <div className="Form-item">
                    <label htmlFor="name">Name:</label>
                    <input
                        type="text"
                        id="company_name"
                        name="company_name"
                        value={publisherData.company_name}
                        onChange={handleChange}
                        placeholder="Name"
                    />
                </div>
                <div className="Form-item">
                    <label htmlFor="website">Website:</label>
                    <input
                        type="text"
                        id="website"
                        name="website"
                        value={publisherData.website}
                        onChange={handleChange}
                        placeholder="Website URL"
                    />
                </div>
                <div className="Form-item">
                    <label htmlFor="location">Location:</label>
                    <input
                        type="text"
                        id="location"
                        name="location"
                        value={publisherData.location}
                        onChange={handleChange}
                        placeholder="Location"
                    />
                </div>
                <button className="Button" type="submit">
                    Submit
                </button>
            </form>
            <p>{result}</p>
        </div>
    );
}

export default AddPublisherForm;
