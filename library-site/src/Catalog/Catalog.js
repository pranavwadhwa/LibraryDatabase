import React, { useState, useEffect } from "react";
import "./Catalog.css";
import "../index.css";
import NavigationBar from "../Navigation/Navigation";
import Network from "../Network";

function BookView(book) {
    return (
        <a
            className="Book-container"
            href={`/book?isbn=${book.isbn}`}
            key={book.book_id}
        >
            <img
                className="Book-image"
                src={book.cover_photo_url}
                alt={book.name}
            />
            <h4 className="Book-title">{book.name}</h4>
            <h6 className="Book-author">{book.author}</h6>
        </a>
    );
}

function App() {
    const [books, setBooks] = useState([]);
    const [visibleBooks, setVisibleBooks] = useState([]);
    const [input, setInput] = useState("");
    let fetched = false;

    useEffect(() => {
        if (fetched) return;
        console.log("Initial value = ", localStorage.getItem("searchvalue"));
        fetched = true;
        Network.getBooks().then((books) => {
            let isbns = new Set(books.map(book => book.isbn));
            let filtered = [];
            books.forEach(element => {
                if (isbns.has(element.isbn)) {
                    filtered.push(element);
                    isbns.delete(element.isbn);
                }
            });
            setBooks(filtered);
            setVisibleBooks(filtered);
        });
    }, []);

    const onInputChange = (e) => {
        setInput(e.target.value);
        setVisibleBooks(
            books.filter((book) =>
                book.name.toLowerCase().includes(e.target.value.toLowerCase())
            )
        );
        localStorage.setItem("searchvalue", e.target.value);
    };

    return (
        <div className="App">
            {NavigationBar()}
            <h1>Library Catalog</h1>
            <input
                className="Catalog-searchBar"
                value={input}
                onChange={onInputChange}
                placeholder="Search for a book title..."
            />
            <div className="Catalog-list">
                {visibleBooks.map((book) => BookView(book))}
            </div>
        </div>
    );
}

export default App;
