import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./index.css";
import Catalog from "./Catalog/Catalog";
import LogIn from "./LogIn/LogIn";
import Book from "./Book/Book";
import AddBook from "./AddBook/AddBook";
import Authors from "./Authors/Authors";
import DeleteBookForm from "./DeleteBook/DeleteBook";
import reportWebVitals from "./reportWebVitals";
import AddAuthorForm from "./AddAuthor/AddAuthor";
import Publishers from "./Publishers/Publishers";
import AddPublisherForm from "./AddPublisher/AddPublisher";
import SignUp from "./SignUp/SignUp";
import Account from "./Account/Account";
import DeleteAuthorForm from "./DeleteAuthor/DeleteAuthor";
import DeletePublisherForm from "./DeletePublisher/DeletePublisher";
import Stats from "./Stats/Stats";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <React.StrictMode>
        <Router>
            <Routes>
                <Route exact path="/" element={<Catalog />} />
                <Route exact path="/add-book" element={<AddBook />} />
                <Route exact path="/add-author" element={<AddAuthorForm />} />
                <Route exact path="/add-publisher" element={<AddPublisherForm />} />
                <Route exact path="/delete-book" element={<DeleteBookForm />} />
                <Route exact path="/delete-author" element={<DeleteAuthorForm />} />
                <Route exact path="/delete-publisher" element={<DeletePublisherForm />} />
                <Route exact path="/log-in" element={<LogIn />} />
                <Route exact path="/sign-up" element={<SignUp />} />
                <Route exact path="/book" element={<Book />} />
                <Route exact path="/authors" element={<Authors />} />
                <Route exact path="/publishers" element={<Publishers />} />
                <Route exact path="/account" element={<Account />} />
                <Route exact path="/stats" element={<Stats />} />
            </Routes>
        </Router>
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
