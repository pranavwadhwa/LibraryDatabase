const baseURL = "http://pranavwadhwavt.pythonanywhere.com";

async function getBooks() {
    return await fetch(`${baseURL}/get_books`).then((response) =>
        response.json()
    );
}

async function getBook(isbn) {
    return await fetch(`${baseURL}/get_book?isbn=${isbn}`).then((response) =>
        response.json()
    );
}

async function getAuthors() {
    return await fetch(`${baseURL}/get_authors`).then((response) =>
        response.json()
    );
}

async function getPublishers() {
    return await fetch(`${baseURL}/get_publishers`).then((response) =>
        response.json()
    );
}

async function addBook(bookData) {
    const u = new URLSearchParams(bookData).toString();
    return await fetch(`${baseURL}/insert_book?${u}`).then((response) =>
        response.text()
    );
}

async function addAuthor(authorData) {
    const u = new URLSearchParams(authorData).toString();
    return await fetch(`${baseURL}/insert_author?${u}`).then((response) =>
        response.text()
    );
}

async function addPublisher(publisherData) {
    const u = new URLSearchParams(publisherData).toString();
    return await fetch(`${baseURL}/insert_publisher?${u}`).then((response) =>
        response.text()
    );
}

async function deleteBook(bookId) {
    return await fetch(`${baseURL}/delete_book?book_id=${bookId}`).then(
        (response) => response.text()
    );
}

async function deleteAuthor(authorName) {
    return await fetch(
        `${baseURL}/delete_author?authorName=${authorName}`
    ).then((response) => response.text());
}

async function deletePublisher(name) {
    return await fetch(`${baseURL}/delete_publisher?company_name=${name}`).then(
        (response) => response.text()
    );
}

async function signUp(name, username, password) {
    return await fetch(
        `${baseURL}/sign_up?memberName=${name}&memberId=${username}&password=${password}`
    ).then((response) => {
        return response.text();
    });
}

async function logIn(username, password) {
    return await fetch(
        `${baseURL}/log_in?memberId=${username}&password=${password}`
    ).then((response) => {
        return response.text();
    });
}

async function changePassword(username, oldPassword, newPassword) {
    return await fetch(
        `${baseURL}/change_password?memberId=${username}&password=${oldPassword}&new_password=${newPassword}`
    ).then((response) => {
        return response.text();
    });
}

async function borrowBook(username, isbn) {
    return await fetch(
        `${baseURL}/checkout_book?memberId=${username}&isbn=${isbn}`
    ).then((response) => {
        return response.text();
    });
}

async function returnBook(username, isbn) {
    return await getHistory(username).then((history) => {
        let book_id = "";
        history.forEach((book) => {
            if (book.split("-")[0] == isbn && book_id.length == 0) {
                book_id = book;
            }
        });

        return fetch(`${baseURL}/return_book?book_id=${book_id}`).then(
            (response) => {
                return response.text();
            }
        );
    });
}

async function getHistory(username) {
    return await fetch(`${baseURL}/get_history?memberId=${username}`).then(
        (response) => {
            return response.json();
        }
    );
}

async function getStat1() {
    return await fetch(`${baseURL}/stat1`)
        .then((response) => response.text())
        .then((s) => Number(s));
}

async function getStat2() {
    return await fetch(`${baseURL}/stat2`)
        .then((response) => response.text())
        .then((s) => Number(s));
}

async function getStat3() {
    return await fetch(`${baseURL}/stat3`)
        .then((response) => response.text())
        .then((s) => Number(s));
}

const Network = {
    getBooks,
    getAuthors,
    getPublishers,
    addBook,
    addAuthor,
    deleteBook,
    deleteAuthor,
    addPublisher,
    deletePublisher,
    signUp,
    logIn,
    changePassword,
    getBook,
    borrowBook,
    returnBook,
    getHistory,
    getStat1,
    getStat2,
    getStat3
};
export default Network;
