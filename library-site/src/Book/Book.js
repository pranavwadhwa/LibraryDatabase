import React, { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import "./Book.css";
import "../index.css";
import NavigationBar from "../Navigation/Navigation";
import Network from "../Network";

function Book() {
    const isbn = useSearchParams()[0].get("isbn");
    const username = localStorage.getItem("username");
    // const book = {
    //     author: "Aldous Huxley",
    //     book_id: "9780060850524-0",
    //     cover_photo_url:
    //         "https://d3525k1ryd2155.cloudfront.net/f/524/850/9780060850524.OL.0.m.jpg",
    //     isbn: "9780060850524",
    //     name: "Brave New World",
    //     publishing_company: "Harper Perennial Modern Classics",
    //     year_published: 1932,
    //     numberAvailableCopies: 4,
    //     current_borrower: "basdf",
    // };
    const [book, setBook] = useState({});
    const [fetched, setFetched] = useState(false);

    useEffect(() => {
        if (fetched) return;
        setFetched(true);
        Network.getBook(isbn).then((book) => {
            console.log(book);
            setBook(book);
        })
    });

    function returnBook() {
        Network.returnBook(username, isbn).then((response) => {
            console.log('returned book', response);
            window.location.reload();
        })
    }

    function borrowBook() {
        Network.borrowBook(username, isbn).then((response) => {
            window.location.reload();
        })
    }

    return (
        <div className="App">
            <NavigationBar />
            <img className="Book-image" src={book.cover_photo_url} />
            <h2 className="Book-title">{book.name}</h2>
            <h3 className="Book-author">{book.author}</h3>
            <p className="Book-info">
                Published by {book.publishing_company} in {book.year_published}
                <br />
                ISBN: {book.isbn}
                <br />
                {book.numberAvailableCopies} copies available
            </p>
            {book.currentBorrowers && book.currentBorrowers.includes(username) && (
                <button className="Button" onClick={returnBook}>Return Book</button>
                )}
            {book.currentBorrowers && !book.currentBorrowers.includes(username) && book.numberAvailableCopies && (
                <button className="Button" onClick={borrowBook}>Borrow Book</button>
            )}
        </div>
    );
}

export default Book;
