import React, { useEffect, useState } from "react";
import "./Stats.css";
import "../index.css";
import NavigationBar from "../Navigation/Navigation";
import Network from "../Network";

function Stats() {
    const [percentage, setPercentage] = useState(0);
    const [numHistory, setNumHistory] = useState(0);
    const [numUsers, setNumUsers] = useState(0);
    const [fetched, setFetched] = useState(false);

    useEffect(() => {
        if (fetched) return;
        setFetched(true);
        Network.getStat1().then((percentage) => {
            setPercentage(percentage);
        });
        Network.getStat2().then((percentage) => {
            setNumHistory(percentage);
        });
        Network.getStat3().then((percentage) => {
            setNumUsers(percentage);
        });
    });

    const division = Math.round(numHistory / numUsers * 100) / 100

    return (
        <div className="App">
            <NavigationBar />
            <div className="Stats-container">
                <h3 className="header">Current borrowing: {percentage}%</h3>
                <h6 className="subheader">Percentage of books currently checked out is {percentage}%</h6>
                <div className="slider-unfilled">
                    <div className="slider-filled" style={{width: `${percentage}%`}}></div>
                </div>
                <h3 className="header">Total check-outs: {numHistory}</h3>
                <h6 className="subheader">There have been {numHistory} total check-outs of any book.</h6>

                <h3 className="header">Number of members: {numUsers}</h3>
                <h6 className="subheader">There are currently {numUsers} people that have created an account.</h6>

                <h3 className="header">Checkouts per member: {division}</h3>
                <h6 className="subheader">On average, each member has checked out {division} books.</h6>
            </div>
        </div>
    );
}

export default Stats;
