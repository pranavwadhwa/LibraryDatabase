import "./Navigation.css";

function AuthBar() {
    if (localStorage.getItem("username") === null) {
        return (
            <div>
                <a className="Navigation-link" href="/log-in">
                    Log In
                </a>
                <a className="Navigation-link" href="/sign-up">
                    Sign Up
                </a>
            </div>
        );
    } else {
        return (
            <a className="Navigation-link" href="/account">
                Account ({localStorage.getItem("username")})
            </a>
        );
    }
}

function NavigationBar() {
    return (
        <div className="Navigation">
            <p className="Navigation-title">Library Database System</p>
            <div className="Navigation-spacer" />
            <a className="Navigation-link" href="/">
                Catalog
            </a>
            <a className="Navigation-link" href="/authors">
                Authors
            </a>
            <a className="Navigation-link" href="/publishers">
                Publishers
            </a>
            {AuthBar()}
        </div>
    );
}

export default NavigationBar;
