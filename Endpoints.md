# Endpoints

A list of all endpoints and their requirements.

### `/get_books`

Gets all the books in the catalog.

- Params: None
- Return: Array of JSON objects of the unique books (object doesn't need book_id)

### `/insert_book`

Adds a book to the catalog.

- Params: `book_id, isbn, name, author, cover_photo_url, year_published, publishing_company`
- Return: None

### `/delete_book`

Deletes a book from the catalog.

- Params: `book_id`
- Return: None

### `/get_book`

Returns information about a specific book

- Param: `book_id`
- Return: JSON object of the book info

### `/insert_author`

Adds a new author to the table.

- Params: `name, bio`
- Return: None

### `/delete_author`

Deletes an author from the table.

- Params: `name`
- Return: None

### `/get_authors`

Gets the list of authors

- Params: None
- Return: Array of JSON objects of the authors

### `/insert_publisher`

Adds a new publisher to the table.

- Params: `company_name, website, location`
- Return: None

### `/delete_publisher`

Deletes an publisher from the table.

- Params: `company_name`
- Return: None

### `/get_publishers`

Gets the list of publishers

- Params: None
- Return: Array of JSON objects of the publishers

### `/sign_up`

Creates an account for a new member. 

- Params: `name, username, password`
- Return: None
- Error: if someone is already using `username`

### `/log_in`

Signs a member into an account

- Params: `username, password`
- Return: None
- Error: if the credentials are not valid

### `/get_user`

Gets information about a specific user

- Params: `username`
- Return: JSON object: `{username, name, borrowing_history (string array of `book_id`)}`

### `/change_password`

Changes a password for a member id

- Params: `username, old_password, new_password`
- Return: None
- Error: if `{username, old_password}` credentials were not valid

### `/checkout_book`

Checks out a book for a specific user. Adds that book to their `borrowing_history` and marks that book's `current_borrower` as this user's username. Find the first copy of that book with the specified ISBN, and use that book_id.

- Params: `username, isbn`
- Return: `book_id`

### `/return_book`

Marks a book as free (i.e. `current_borrower` is null).

- Params: `book_id`
- Return: None
