import mysql.connector
import csv
from datetime import date
from flask import Flask, request, jsonify
from flask_cors import CORS

app = Flask(__name__)
# CORS(app)
CORS(app, resources={r"/*": {"origins": "*"}})

def get_cursor():
    # establish a connection to the MySQL server
    cnx = mysql.connector.connect(
        user='pranavwadhwavt',
        password='catalog_vt_database',
        # host='172.17.3.124',
        host='pranavwadhwavt.mysql.pythonanywhere-services.com',
        database='pranavwadhwavt$Library_catalog'
    )

    # define a cursor object to execute queries
    return cnx.cursor(), cnx

# insert a new book into the books table
insert_book_query = "INSERT INTO books (book_id, isbn, name, author, cover_photo_url, year_published, publishing_company, current_borrower) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
delete_book_query = "DELETE FROM books WHERE book_id = %s"

def load_initial_books():
    csv_data = csv.reader(open('sampledata.csv'))
    cursor, cnx = get_cursor()
    for row in csv_data:
        row[5] = int(row[5])
        print(row, len(row))
        cursor.execute(insert_book_query, tuple(row))
        cnx.commit()

def clear_all_books():
    cursor, cnx = get_cursor()
    delete_all_book_query = "DELETE FROM books"
    cursor.execute(delete_all_book_query)
    cnx.commit()

@app.route('/reset')
def reset():
    clear_all_books()
    load_initial_books()
    return 'success'

@app.route('/get_books')
def get_books():
    cursor, cnx = get_cursor()
    fetch_books_query = "SELECT * FROM books"
    cursor.execute(fetch_books_query)
    result = cursor.fetchall()
    books = []
    for book in result:
        books.append({"book_id": book[0], "isbn": book[1], "name": book[2],  "author": book[3], "cover_photo_url": book[4], "year_published": book[5], "publishing_company": book[6], "current_borrower": book[8]})
    return jsonify(books)

@app.route('/get_book')
def get_book():
    cursor, cnx = get_cursor()
    isbn = request.args.get('isbn')
    fetch_books_query = "SELECT * FROM books WHERE isbn = %s"
    cursor.execute(fetch_books_query, (isbn,))
    result = cursor.fetchall()
    if len(result) == 0:
        return None
    numAvailable = 0
    currentBorrowers = []
    for book in result:
        print(book[0], book[8])
        if book[8] == None:
            numAvailable += 1
        else:
            currentBorrowers.append(book[8])
    book = result[0]
    return jsonify({"currentBorrowers": currentBorrowers, "numberAvailableCopies": numAvailable,  "isbn": book[1], "name": book[2],  "author": book[3], "cover_photo_url": book[4], "year_published": book[5], "publishing_company": book[6]})

@app.route('/insert_book')
def insert_book():
    cursor, cnx = get_cursor()
    book_id = request.args.get('book_id')
    isbn = request.args.get('isbn')
    name = request.args.get('name')
    author = request.args.get('author')
    cover_photo_url = request.args.get('cover_photo_url')
    year_published = int(request.args.get('year_published'))
    publishing_company = request.args.get('publishing_company')
    current_borrower = request.args.get('current_borrower')
    cursor.execute(insert_book_query, (book_id, isbn, name, author, cover_photo_url, year_published, publishing_company, current_borrower))
    cnx.commit()
    return str('success')

@app.route('/delete_book')
def delete_book():
    cursor, cnx = get_cursor()
    book_id = request.args.get('book_id')
    cursor.execute(delete_book_query, (book_id,))
    cnx.commit()
    return str('success')

insert_author_query = "INSERT INTO Authors (authorName, bio) VALUES (%s, %s)"
delete_author_query = "DELETE FROM Authors WHERE authorName = %s"

@app.route('/insert_author')
def insert_author():
    cursor, cnx = get_cursor()
    authorName = request.args.get('name')
    bio = request.args.get('bio')
    cursor.execute(insert_author_query, (authorName, bio))
    cnx.commit()
    return str('success')

@app.route('/delete_author')
def delete_author():
    cursor, cnx = get_cursor()
    authorName = request.args.get('authorName')
    cursor.execute(delete_author_query, (authorName,))
    cnx.commit()
    return str('success')

@app.route('/get_authors')
def get_authors():
    cursor, cnx = get_cursor()
    fetch_author_query = "SELECT * FROM Authors"
    cursor.execute(fetch_author_query)
    result = cursor.fetchall()
    authors = []
    for author in result:
        authors.append({"authorName": author[0], "bio": author[1]})
    return jsonify(authors)

insert_publisher_query = "INSERT INTO publisher (name, location, website) VALUES (%s, %s, %s )"
delete_publisher_query = "DELETE FROM publisher WHERE name = %s"

@app.route('/insert_publisher')
def insert_publisher():
    cursor, cnx = get_cursor()
    company_name = request.args.get('company_name')
    website = request.args.get('website')
    location = request.args.get('location')

    cursor.execute(insert_publisher_query, (company_name, website, location))
    cnx.commit()
    return str('success')

@app.route('/delete_publisher')
def delete_publisher():
    cursor, cnx = get_cursor()
    company_name = request.args.get('company_name')
    cursor.execute(delete_publisher_query, (company_name,))
    cnx.commit()
    return str('success')

@app.route('/get_publishers')
def get_publishers():
    cursor, cnx = get_cursor()
    fetch_publisher_query = "SELECT * FROM publisher"
    cursor.execute(fetch_publisher_query)
    result = cursor.fetchall()
    publishers = []
    for publisher in result:
        publishers.append({"publisher_id": publisher[0], "name": publisher[1], "location": publisher[3], "website": publisher[2]})
    return jsonify(publishers)

insert_member_query = "INSERT INTO Members (memberId, memberName, password) VALUES (%s, %s, %s)"
check_username_query = "SELECT * FROM Members where memberId = %s"
delete_member_query = "DELETE FROM Members WHERE memberId = %s"

@app.route('/sign_up')
def sign_up():
    cursor, cnx = get_cursor()
    memberName = request.args.get('memberName')
    memberId = request.args.get('memberId')
    password = request.args.get('password')
    cursor.execute(check_username_query, (memberId,))
    result = cursor.fetchall()
    if len(result) > 0:
        return str('This username is already being used.')
    cursor.execute(insert_member_query, (memberId, memberName, password))
    cnx.commit()
    return str('success')

@app.route('/stat1')
def stat1():
    cursor, cnx = get_cursor()
    cursor.execute('SELECT COUNT(*) * 100 / (SELECT COUNT(*) FROM books) AS percentage_checked_out FROM books WHERE current_borrower IS NOT NULL;')
    result = cursor.fetchall()
    return str(result[0][0])

@app.route('/stat2')
def stat2():
    cursor, cnx = get_cursor()
    cursor.execute('SELECT COUNT(*) FROM borrowing_history;')
    result = cursor.fetchall()
    return str(result[0][0])

@app.route('/stat3')
def stat3():
    cursor, cnx = get_cursor()
    cursor.execute('SELECT COUNT(*) FROM Members;')
    result = cursor.fetchall()
    return str(result[0][0])

@app.route('/get_user')
def get_user(memberId):
    cursor, cnx = get_cursor()
    fetch_member_query = "SELECT * FROM Members"
    cursor.execute(fetch_member_query)
    result = cursor.fetchall()
    wanted_member = None
    for member in result:
        if member[0] == memberId:
            wanted_member = {"memberId": member[0], "memberName": member[1], "password": member[2]}
    if wanted_member == None:
        return('cannot find user')
    else:
        return jsonify(wanted_member)

@app.route('/log_in')
def log_in():
    cursor, cnx = get_cursor()
    fetch_member_query = "SELECT * FROM Members"
    memberId = request.args.get('memberId')
    password = request.args.get('password')
    cursor.execute(fetch_member_query)
    result = cursor.fetchall()
    for member in result:
        if member[0] == memberId and member[2] == password:
            return str('signed user into account')
    return str('error: invalid credentials')

change_password_query = "UPDATE Members SET password = %s WHERE memberId = %s"

@app.route('/change_password')
def change_password():
    cursor, cnx = get_cursor()
    fetch_member_query = "SELECT * FROM Members"
    memberId = request.args.get('memberId')
    password = request.args.get('password')
    new_password = request.args.get('new_password')
    cursor.execute(fetch_member_query)
    result = cursor.fetchall()
    for member in result:
        if member[0] == memberId and member[2] == password:
            cursor.execute(change_password_query, (new_password, memberId))
            cnx.commit()
            return('sucessfuly changed password')
    return('username and password do not match')

insert_borrowing_history_query = "INSERT INTO borrowing_history (member_id, book_id) VALUES (%s, %s)"
update_current_borrower_query = "UPDATE books SET current_borrower = %s WHERE book_id = %s"

@app.route('/checkout_book')
def checkout_book():
    cursor, cnx = get_cursor()
    fetch_member_query = "SELECT * FROM Members"
    fetch_books_query = "SELECT * FROM books WHERE current_borrower IS NULL"
    memberId = request.args.get('memberId')
    isbn = request.args.get('isbn')
    cursor.execute(fetch_member_query)
    result = cursor.fetchall()
    wanted_member = {}
    foundMember = False
    for member in result:
        if member[0] == memberId:
            wanted_member = {"memberId": member[0], "memberName": member[1], "password": member[2]}
            foundMember = True
    cursor.execute(fetch_books_query)
    result = cursor.fetchall()
    wanted_book = {}
    foundBook = False
    for book in result:
        if book[1] == isbn:
            foundBook = True
            wanted_book = {"book_id": book[0], "isbn": book[1], "name": book[2],  "author": book[3], "cover_photo_url": book[4], "year_published": book[5], "publishing_company": book[6], "current_borrower": book[8]}
    if foundBook and foundMember:
        member_id = wanted_member["memberId"]
        book_id = wanted_book["book_id"]
        cursor.execute(insert_borrowing_history_query, (member_id, book_id))
        cursor.execute(update_current_borrower_query, (member_id, book_id))
        cnx.commit()
        return str('success')
    else:
        return str('book is already checked out')

modify_book_query = "update books set current_borrower = NULL where book_id = %s"

@app.route('/return_book')
def return_book():
    cursor, cnx = get_cursor()
    fetch_books_query = "SELECT * FROM books"
    book_id = request.args.get('book_id')
    cursor.execute(fetch_books_query)
    result = cursor.fetchall()
    for book in result:
        if book[0] == book_id:
            cursor.execute(modify_book_query, (book_id,))
            cnx.commit()
            return str('success')
    return str('book not returned')

@app.route('/get_history')
def get_history():
    cursor, cnx = get_cursor()
    member_id = request.args.get('memberId')
    query = "SELECT * FROM borrowing_history WHERE member_id = %s"
    cursor.execute(query, (member_id,))
    result = cursor.fetchall()
    history = []
    for r in result:
        history.append(r[2])
    return jsonify(history)

if __name__ == "__main__":
    app.run()