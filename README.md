# LibraryDatabase

### Frontend

To run the React app, run:

```
cd library-site
npm start
```

### Database setup

Install MySQL:
```bash
brew install mysql
```

Add this line to `/opt/homebrew/etc/my.cnf`
```bash
local-infile=1
```

Then run
```bash
mysql_secure_installation # Enter a password and skip everything else
mysql -u root -p # Enter your password
```

In the SQL cli:

```MySQL
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
flush privileges

CREATE DATABASE library_catalog;
USE library_catalog;
CREATE TABLE books (
    book_id VARCHAR(20) NOT NULL,
    isbn VARCHAR(13) NOT NULL,
    name VARCHAR(255) NOT NULL,
    author VARCHAR(255) NOT NULL,
    cover_photo_url VARCHAR(255),
    year_published INT(4),
    publishing_company VARCHAR(255),
    PRIMARY KEY (book_id),
    CHECK (isbn REGEXP '^[0-9]{13}$'),
    CHECK (book_id REGEXP '^[0-9]{13}-[0-9]+$')
  );
```

### Backend
Here are the list of endponts

- `/get_books`: returns a JSON object of all books in the table
- `/insert_book`: adds a book to the table. Required params: `book_id`, `isbn`, `name`, `author`, `cover_photo_url`, `year_published`, `publishing_company`
- `/delete_book`: deletes a book from the table. Required params: `book_id`
